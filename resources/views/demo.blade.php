preprobation.html
Who has access

P
System properties
Type
HTML
Size
16 KB
Storage used
16 KB
Location
Probation source code
Owner
Muhammad Rifqy Fakhrul Hadi
Modified
10:10 AM by Muhammad Rifqy Fakhrul Hadi
Opened
5:30 PM by me
Created
10:10 AM
Add a description
Viewers can download
<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head><base href="../">
    <title>Metronic - the world's #1 selling Bootstrap Admin Theme Ecosystem for HTML, Vue, React, Angular &amp; Laravel by Keenthemes</title>
    <meta charset="utf-8" />
    <meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="https://preview.keenthemes.com/metronic8/demo3/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="https://preview.keenthemes.com/metronic8/demo3/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->

    <style>
        .ibet-teams a{
            font-weight: 500;
            font-size: 24px;
        }
    </style>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body">
<!--begin::Main-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="page d-flex flex-row flex-column-fluid">
        <!--begin::Wrapper-->
        <div class="flex-row-fluid" id="kt_wrapper">
            <!--begin::Header-->

            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Container-->
                <div class="container-xxl" id="kt_content_container">
                    <!--end::Row-->
                    <!--begin::Row-->

                    <!--end::Row-->
                    <!--begin::Row-->
                    <div class="row gy-10 g-xl-10">
                        <!--begin::Col-->
                        <div class="col-xxl mb-5 mb-lg-6">
                            <!--begin::Chart widget 13-->
                            <div class="card card-flush h-md-150">
                                <!--begin::Header-->
                                <div class="card-header pt-7">
                                    <!--begin::Title-->
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder text-dark">Sales Statistics</span>
                                        <span class="text-gray-400 pt-2 fw-bold fs-6">Top Selling Products</span>
                                    </h3>
                                    <!--end::Title-->
                                    <!--begin::Toolbar-->
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                            <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
															<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														</svg>
													</span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-50 py-4" data-kt-menu="true">
                                            <div class="row">
                                                <div class="col-lg-6 h-10">
                                                    <a href="#" class="menu-link px-3">Location Type</a>
                                                    <div class="row p-2 location-type"></div>
                                                </div>
                                                <div class="col-lg-6 h-10">
                                                    <a href="#" class="menu-link px-3">Teams</a>
                                                    <div class="row p-2 ibet-teams">
                                                        <div class="col">
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 ">IBet 1</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pt-2">
                                    <!--begin::Chart container-->
                                    <div id="mainChart" class="w-200" style="height: 70vh;"></div>
                                    <!--end::Chart container-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Chart widget 13-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                    <!--begin::Row-->

                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row flex-stack">
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::Root-->
<!--end::Main-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
    <span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
    <!--end::Svg Icon-->
</div>
<script>var hostUrl = "assets/";</script>
<script src="https://preview.keenthemes.com/metronic8/demo3/assets/plugins/global/plugins.bundle.js"></script>
<script src="https://preview.keenthemes.com/metronic8/demo3/assets/js/scripts.bundle.js"></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>

<script>

    // preventDefault on click anchor
    $(document).on('click', 'a', function (e) {
        e.preventDefault();
    });

    let wrapper = ""
    let comp = ""

    for (let i = 1; i <= 16; i++) {
        comp += `<div class="menu-item px-3">
                  <a href="#" class="menu-link px-3">City ${i}</a>
                </div>`

        if ([8,16].includes(i)) {
            wrapper += `<div class="col-6">${comp}</div>`
            comp = ""
        }
    }

    document.getElementsByClassName("location-type")[0].innerHTML = wrapper;

    var e = document.getElementById("mainChart");
    e &&
    am5.ready(function () {
        var t = am5.Root.new(e);
        t.setThemes([am5themes_Animated.new(t)]);
        var cellSize = 30;
        t.events.on("datavalidated", function(ev) {

            // Get objects of interest
            var chart = ev.target;
            var categoryAxis = chart.yAxes.getIndex(0);

            // Calculate how we need to adjust chart height
            var adjustHeight = chart.data.length * cellSize - categoryAxis.pixelHeight;

            // get current chart height
            var targetHeight = chart.pixelHeight + adjustHeight;

            // Set it on chart's container
            chart.svgContainer.htmlElement.style.height = targetHeight + "px";
        });
        var a = t.container.children.push(
            am5xy.XYChart.new(t, {
                panX: !0,
                panY: !0,
                wheelX: "panX",
                wheelY: "zoomX",
            })
        );
        a.set(
            "cursor",
            am5xy.XYCursor.new(t, { behavior: "none" })
        ).lineY.set("visible", !1);
        var r = [
                { year: "2021 jan", lundu: 51,lundu1: 20,lundu2: 20, bau: 63, Pandawan: 74,serian:21, balai_ringin:77, sri_aman:47, lubok_antu:12, song:38, bukit_bintang:74, belaga:25 , marudi: 30, lawas: 26,limbang:15},
                { year: "2021 feb", lundu: 59, bau: 72, Pandawan: 85,serian:24, balai_ringin:89, sri_aman:54, lubok_antu:14, song:44, bukit_bintang:85, belaga:29 , marudi: 35, lawas: 30,limbang:17},
                { year: "2021 Mar", lundu: 67, bau: 83, Pandawan: 98,serian:28, balai_ringin:102, sri_aman:62, lubok_antu:16, song:55, bukit_bintang:98, belaga:33 , marudi: 40, lawas: 34,limbang:20},
                { year: "2021 Apr", lundu: 64, bau: 79, Pandawan: 93,serian:26, balai_ringin:97, sri_aman:59, lubok_antu:15, song:48, bukit_bintang:93, belaga:31 , marudi: 38, lawas: 33,limbang:19},
                { year: "2021 May", lundu: 61, bau: 75, Pandawan: 88,serian:25, balai_ringin:92, sri_aman:56, lubok_antu:14, song:45, bukit_bintang:88, belaga:30 , marudi: 36, lawas: 31,limbang:18},
                { year: "2021 Jun", lundu: 70, bau: 86, Pandawan: 102,serian:29, balai_ringin:106, sri_aman:65, lubok_antu:16, song:52, bukit_bintang:102, belaga:34 , marudi: 41, lawas: 36,limbang:21},
                { year: "2021 Jul", lundu: 56, bau: 69, Pandawan: 81,serian:23, balai_ringin:85, sri_aman:52, lubok_antu:13, song:42, bukit_bintang:81, belaga:27 , marudi: 33, lawas: 29,limbang:16},
                { year: "2021 Aug", lundu: 64, bau: 80, Pandawan: 93,serian:27, balai_ringin:97, sri_aman:59, lubok_antu:15, song:48, bukit_bintang:93, belaga:32 , marudi: 38, lawas: 33,limbang:19},
            ],
            o = a.xAxes.push(
                am5xy.CategoryAxis.new(t, {
                    categoryField: "year",
                    startLocation: 0.5,
                    endLocation: 0.5,
                    renderer: am5xy.AxisRendererX.new(t, {}),
                    tooltip: am5.Tooltip.new(t, {}),
                })
            );
        o
            .get("renderer")
            .grid.template.setAll({ disabled: !0, strokeOpacity: 0 }),
            o
                .get("renderer")
                .labels.template.setAll({
                fontWeight: "400",
                fontSize: 13,
                fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-500")),
            }),
            o.data.setAll(r);
        var i = a.yAxes.push(
            am5xy.ValueAxis.new(t, {
                renderer: am5xy.AxisRendererY.new(t, {}),
            })
        );
        function s(e, s, l) {
            var n = a.series.push(
                am5xy.LineSeries
                    .new(t, {
                        name: e,
                        xAxis: o,
                        yAxis: i,
                        stacked: !0,
                        valueYField: s,
                        categoryXField: "year",
                        fill: am5.color(l),
                        // size chart

                        tooltip: am5.Tooltip.new(t, {
                            pointerOrientation: "horizontal",
                            labelText:"[bold]{name}[/]\n{categoryX}: {valueY}",

                        }),
                    })
            );
            n.fills.template.setAll({ fillOpacity: 0.5, visible: !0 }),
                n.data.setAll(r),
                n.appear(1e3);
        }
        i
            .get("renderer")
            .grid.template.setAll({
            stroke: am5.color(KTUtil.getCssVariableValue("--bs-gray-300")),
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeDasharray: [3],
        }),
            i
                .get("renderer")
                .labels.template.setAll({
                fontWeight: "400",
                fontSize: 13,
                fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-500")),
            }),
            s("Lundu", "lundu", KTUtil.getCssVariableValue("--bs-primary")),
            s("Bau", "bau", KTUtil.getCssVariableValue("--bs-info")),
            s(
                "Pandawan",
                "pandawan",
                KTUtil.getCssVariableValue("--bs-success")
            ),
            s(
                "Serian",
                "serian",
                KTUtil.getCssVariableValue("--bs-warning")
            ),
            s("Balai Ringin", "balai_ringin", "#E86E25"/*KTUtil.getCssVariableValue("--bs-primary")*/),
            s("Sri Aman", "sri_aman", "##700F00"),
            s(
                "Lubok Antu",
                "lubok_antu",
                "#24e5cf"
            ),
            s(
                "Song",
                "song",
                "#ef6b6b"
            ),
            s("Bukit Bintang", "bukit_bintang", "#A17771"),
            s(
                "Belaga",
                "belaga",
                "#2c8ef7"
            ),
            s(
                "Marudi",
                "marudi",
                "#b6ea25"
            ),
            s(
                "Lawas",
                "lawas",
                "#609e58"
            ),
            s(
                "Limbang",
                "limbang",
                "#340eb2"
            ),
            a.set(
                "scrollbarX",
                am5.Scrollbar.new(t, {
                    orientation: "horizontal",
                    marginBottom: 25,
                    height: 8,
                })
            );
        var l = o.makeDataItem({ category: "2021 jan", endCategory: "2021 aug" });
        o.createAxisRange(l),
            l
                .get("grid")
                .setAll({
                    stroke: am5.color(
                        KTUtil.getCssVariableValue("--bs-gray-200")
                    ),
                    strokeOpacity: 0.5,
                    strokeDasharray: [3],
                }),
            l
                .get("axisFill")
                .setAll({
                    fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-200")),
                    fillOpacity: 0.1,
                }),
            l
                .get("label")
                .setAll({
                    inside: !0,
                    text: "Fines increased",
                    rotation: 90,
                    centerX: am5.p100,
                    centerY: am5.p100,
                    location: 0,
                    paddingBottom: 10,
                    paddingRight: 15,
                });
        var n = o.makeDataItem({ category: "2021" });
        o.createAxisRange(n),
            n
                .get("grid")
                .setAll({
                    stroke: am5.color(KTUtil.getCssVariableValue("--bs-danger")),
                    strokeOpacity: 1,
                    strokeDasharray: [3],
                }),
            n
                .get("label")
                .setAll({
                    inside: !0,
                    text: "Fee introduced",
                    rotation: 90,
                    centerX: am5.p100,
                    centerY: am5.p100,
                    location: 0,
                    paddingBottom: 10,
                    paddingRight: 15,
                }),
            a.appear(1e3, 100);
    });
</script>
</body>
<!--end::Body-->
</html>
