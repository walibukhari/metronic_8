<!--begin::Javascript-->
<script>var hostUrl = "/metronic8/demo3/assets/";</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{$base_url_assets}}/assets/plugins/global/plugins.bundle.js"></script>
<script src="{{$base_url_assets}}/assets/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="{{$base_url_assets}}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="{{$base_url_assets}}/assets/plugins/custom/vis-timeline/vis-timeline.bundle.js"></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<script>

    // preventDefault on click anchor
    $(document).on('click', 'a', function (e) {
        e.preventDefault();
    });

    let wrapper = ""
    let comp = ""

    for (let i = 1; i <= 16; i++) {
        comp += `<div class="menu-item px-3">
                  <a href="#" class="menu-link px-3">City ${i}</a>
                </div>`

        if ([8,16].includes(i)) {
            wrapper += `<div class="col-6">${comp}</div>`
            comp = ""
        }
    }

    document.getElementsByClassName("location-type")[0].innerHTML = wrapper;

    var e = document.getElementById("mainChart");
    e &&
    am5.ready(function () {
        var t = am5.Root.new(e);
        t.setThemes([am5themes_Animated.new(t)]);
        var cellSize = 30;
        t.events.on("datavalidated", function(ev) {

            // Get objects of interest
            var chart = ev.target;
            var categoryAxis = chart.yAxes.getIndex(0);

            // Calculate how we need to adjust chart height
            var adjustHeight = chart.data.length * cellSize - categoryAxis.pixelHeight;

            // get current chart height
            var targetHeight = chart.pixelHeight + adjustHeight;

            // Set it on chart's container
            chart.svgContainer.htmlElement.style.height = targetHeight + "px";
        });
        var a = t.container.children.push(
            am5xy.XYChart.new(t, {
                panX: !0,
                panY: !0,
                wheelX: "panX",
                wheelY: "zoomX",
            })
        );
        a.set(
            "cursor",
            am5xy.XYCursor.new(t, { behavior: "none" })
        ).lineY.set("visible", !1);
        var r = [
                { year: "2021 jan", lundu: 51,lundu1: 20,lundu2: 20, bau: 63, Pandawan: 74,serian:21, balai_ringin:77, sri_aman:47, lubok_antu:12, song:38, bukit_bintang:74, belaga:25 , marudi: 30, lawas: 26,limbang:15},
                { year: "2021 feb", lundu: 59, bau: 72, Pandawan: 85,serian:24, balai_ringin:89, sri_aman:54, lubok_antu:14, song:44, bukit_bintang:85, belaga:29 , marudi: 35, lawas: 30,limbang:17},
                { year: "2021 Mar", lundu: 67, bau: 83, Pandawan: 98,serian:28, balai_ringin:102, sri_aman:62, lubok_antu:16, song:55, bukit_bintang:98, belaga:33 , marudi: 40, lawas: 34,limbang:20},
                { year: "2021 Apr", lundu: 64, bau: 79, Pandawan: 93,serian:26, balai_ringin:97, sri_aman:59, lubok_antu:15, song:48, bukit_bintang:93, belaga:31 , marudi: 38, lawas: 33,limbang:19},
                { year: "2021 May", lundu: 61, bau: 75, Pandawan: 88,serian:25, balai_ringin:92, sri_aman:56, lubok_antu:14, song:45, bukit_bintang:88, belaga:30 , marudi: 36, lawas: 31,limbang:18},
                { year: "2021 Jun", lundu: 70, bau: 86, Pandawan: 102,serian:29, balai_ringin:106, sri_aman:65, lubok_antu:16, song:52, bukit_bintang:102, belaga:34 , marudi: 41, lawas: 36,limbang:21},
                { year: "2021 Jul", lundu: 56, bau: 69, Pandawan: 81,serian:23, balai_ringin:85, sri_aman:52, lubok_antu:13, song:42, bukit_bintang:81, belaga:27 , marudi: 33, lawas: 29,limbang:16},
                { year: "2021 Aug", lundu: 64, bau: 80, Pandawan: 93,serian:27, balai_ringin:97, sri_aman:59, lubok_antu:15, song:48, bukit_bintang:93, belaga:32 , marudi: 38, lawas: 33,limbang:19},
            ],
            o = a.xAxes.push(
                am5xy.CategoryAxis.new(t, {
                    categoryField: "year",
                    startLocation: 0.5,
                    endLocation: 0.5,
                    renderer: am5xy.AxisRendererX.new(t, {}),
                    tooltip: am5.Tooltip.new(t, {}),
                })
            );
        o
            .get("renderer")
            .grid.template.setAll({ disabled: !0, strokeOpacity: 0 }),
            o
                .get("renderer")
                .labels.template.setAll({
                fontWeight: "400",
                fontSize: 13,
                fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-500")),
            }),
            o.data.setAll(r);
        var i = a.yAxes.push(
            am5xy.ValueAxis.new(t, {
                renderer: am5xy.AxisRendererY.new(t, {}),
            })
        );
        function s(e, s, l) {
            console.log("e ==>" , e);
            console.log("s ==>" , s);
            console.log("l ==>" , l);
            var n = a.series.push(
                am5xy.LineSeries
                    .new(t, {
                        name: e,
                        xAxis: o,
                        yAxis: i,
                        stacked: !0,
                        valueYField: s,
                        categoryXField: "year",
                        fill: am5.color(l),
                        // size chart

                        tooltip: am5.Tooltip.new(t, {
                            pointerOrientation: "horizontal",
                            labelText:"[bold]{name} , {categoryX}",

                        }),
                    })
            );
            n.fills.template.setAll({ fillOpacity: 0.5, visible: !0 }),
                n.data.setAll(r),
                n.appear(1e3);
        }
        i
            .get("renderer")
            .grid.template.setAll({
            stroke: am5.color(KTUtil.getCssVariableValue("--bs-gray-300")),
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeDasharray: [3],
        }),
            i
                .get("renderer")
                .labels.template.setAll({
                fontWeight: "400",
                fontSize: 13,
                fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-500")),
            }),
            s("Lundu: 51 F: 21 M: 30 (30%)", "lundu", KTUtil.getCssVariableValue("--bs-primary")),
            s("Bau: 69 F:25 M:44 (44%)", "bau", KTUtil.getCssVariableValue("--bs-info")),
            s(
                "Pandawan 53 F: 35 M: 18 (18%)",
                "pandawan",
                KTUtil.getCssVariableValue("--bs-success")
            ),
            s(
                "Serian 73 F: 45 M: 28 (28%)",
                "serian",
                KTUtil.getCssVariableValue("--bs-warning")
            ),
            s("Balai Ringin 60 F: 33 M: 27 (27%)", "balai_ringin", "#E86E25"/*KTUtil.getCssVariableValue("--bs-primary")*/),
            s("Sri Aman 33 F: 17 M: 16 (16%)", "sri_aman", "##700F00"),
            s(
                "Lubok Antu 60 F: 32 M: 28 (28%)",
                "lubok_antu",
                "#24e5cf"
            ),
            s(
                "Song 35 F: 15 M: 20 (20%)",
                "song",
                "#ef6b6b"
            ),
            s("Bukit Bintang 33 F: 14 M: 19 (19%)", "bukit_bintang", "#A17771"),
            s(
                "Belaga 54 F: 28 M: 26 (26%)",
                "belaga",
                "#2c8ef7"
            ),
            s(
                "Marudi 31 F: 13 M: 18 (18%)",
                "marudi",
                "#b6ea25"
            ),
            s(
                "Lawas 33 F: 25 M: 18 (28%)",
                "lawas",
                "#609e58"
            ),
            s(
                "Limbang 40 F: 18 M: 22 (22%)",
                "limbang",
                "#340eb2"
            ),
            a.set(
                "scrollbarX",
                am5.Scrollbar.new(t, {
                    orientation: "horizontal",
                    marginBottom: 25,
                    height: 8,
                })
            );
        var l = o.makeDataItem({ category: "2021 jan", endCategory: "2021 aug" });
        o.createAxisRange(l),
            l
                .get("grid")
                .setAll({
                    stroke: am5.color(
                        KTUtil.getCssVariableValue("--bs-gray-200")
                    ),
                    strokeOpacity: 0.5,
                    strokeDasharray: [3],
                }),
            l
                .get("axisFill")
                .setAll({
                    fill: am5.color(KTUtil.getCssVariableValue("--bs-gray-200")),
                    fillOpacity: 0.1,
                }),
            l
                .get("label")
                .setAll({
                    inside: !0,
                    text: "Fines increased",
                    rotation: 90,
                    centerX: am5.p100,
                    centerY: am5.p100,
                    location: 0,
                    paddingBottom: 10,
                    paddingRight: 15,
                });
        var n = o.makeDataItem({ category: "2021" });
        o.createAxisRange(n),
            n
                .get("grid")
                .setAll({
                    stroke: am5.color(KTUtil.getCssVariableValue("--bs-danger")),
                    strokeOpacity: 1,
                    strokeDasharray: [3],
                }),
            n
                .get("label")
                .setAll({
                    inside: !0,
                    text: "Fee introduced",
                    rotation: 90,
                    centerX: am5.p100,
                    centerY: am5.p100,
                    location: 0,
                    paddingBottom: 10,
                    paddingRight: 15,
                }),
            a.appear(1e3, 100);
    });
</script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{$base_url_assets}}/assets/js/widgets.bundle.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/widgets.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/apps/chat/chat.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/intro.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/type.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/budget.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/settings.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/team.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/targets.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/files.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/complete.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-project/main.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/create-app.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/upgrade-plan.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/new-address.js"></script>
<script src="{{$base_url_assets}}/assets/js/custom/utilities/modals/users-search.js"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
