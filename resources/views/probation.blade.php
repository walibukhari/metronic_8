<style>
    .ibet-teams a{
        font-weight: 500;
        font-size: 24px;
    }
</style>

<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="page d-flex flex-row flex-column-fluid">
        <!--begin::Wrapper-->
        <div class="flex-row-fluid" id="kt_wrapper">
            <!--begin::Header-->

            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Container-->
                <div class="container-xxl" id="kt_content_container">
                    <!--end::Row-->
                    <!--begin::Row-->

                    <!--end::Row-->
                    <!--begin::Row-->
                    <div class="row gy-10 g-xl-10">
                        <!--begin::Col-->
                        <div class="col-xxl mb-5 mb-lg-6">
                            <!--begin::Chart widget 13-->
                            <div class="card card-flush h-md-150">
                                <!--begin::Header-->
                                <div class="card-header pt-7">
                                    <!--begin::Title-->
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder text-dark">All IBET TEAMS</span>
                                        <span style="color:#fff !important;" class="text-gray-400 pt-2 fw-bold fs-6">Mode of Dog removal by district</span>
                                    </h3>
                                    <!--end::Title-->
                                    <!--begin::Toolbar-->
                                    <p style="position: relative;
    left: 231px;
    width: 28%;
    float: left;">The selected 26 days data has increased in 0.5% compare to previous 26days.</p>
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                            <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
															<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														</svg>
													</span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-50 py-4" data-kt-menu="true">
                                            <div class="row">
                                                <div class="col-lg-6 h-10">
                                                    <a href="#" class="menu-link px-3">Location Type</a>
                                                    <div class="row p-2 location-type"></div>
                                                </div>
                                                <div class="col-lg-6 h-10">
                                                    <a href="#" class="menu-link px-3">Teams</a>
                                                    <div class="row p-2 ibet-teams">
                                                        <div class="col">
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 ">IBet 1</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pt-2">
                                    <!--begin::Chart container-->
                                    <div id="mainChart" class="w-200" style="height: 70vh;"></div>
                                    <!--end::Chart container-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Chart widget 13-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                    <!--begin::Row-->

                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row flex-stack">
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
